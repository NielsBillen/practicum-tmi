package io;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import core.Circle;

/**
 * 
 * @author Niels Billen
 * @version 1.0
 */
public class TMIFileParser {
	private final List<Circle> circles = new LinkedList<Circle>();
	private int algorithmId;

	/**
	 * 
	 * @param file
	 */
	public TMIFileParser(File file) {
		FileReader reader = null;
		BufferedReader r = null;

		try {
			reader = new FileReader(file);
			r = new BufferedReader(reader);

			algorithmId = Integer.parseInt(r.readLine());
			int nbOfCircles = Integer.parseInt(r.readLine());

			if (algorithmId < 1 || algorithmId > 3)
				throw new IllegalStateException("unknown algorithm id!");

			String line;
			while ((line = r.readLine()) != null) {
				String[] s = line.split(" +");
				double x = Double.parseDouble(s[0]);
				double y = Double.parseDouble(s[1]);
				double z = Double.parseDouble(s[2]);
				circles.add(new Circle(x, y, z));
			}

			if (circles.size() != nbOfCircles)
				throw new IOException(
						"read number of circles does not match the number of parsed circles!");
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (r != null)
					r.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
			try {
				if (reader != null)
					reader.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * 
	 * @return
	 */
	public List<Circle> getCircles() {
		return new ArrayList<Circle>(circles);
	}

	/**
	 * 
	 * @return
	 */
	public int getAlgorithm() {
		return algorithmId;
	}
}
