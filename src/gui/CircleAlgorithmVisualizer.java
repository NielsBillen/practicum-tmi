package gui;

import core.Circle;
import core.Point;

/**
 * Interface which should be implemented by all elements which would like to
 * visualize a circle intersection algorithm.
 * 
 * @author Niels Billen
 * @version 0.1
 */
public interface CircleAlgorithmVisualizer {
	/**
	 * Set the counter for the number of intersection tests.
	 * 
	 * @param intersectionTests
	 */
	public void setIntersectionTests(int intersectionTests);

	/**
	 * Add an intersection point.
	 * 
	 * @param point
	 */
	public void addPoint(Point point);

	/**
	 * Removes all the points.
	 */
	public void clearPoints();

	/**
	 * Add a highlight for the given circle in the given style.
	 * 
	 * @param circle
	 * @param highlight
	 */
	public void addHighlight(Circle circle, CircleHighlight highlight);

	/**
	 * Removes the highlight for the given circle.
	 * 
	 * @param circle
	 */
	public void removeHighlight(Circle circle);

	/**
	 * Removes all the highlights.
	 */
	public void clearHightlight();

	/**
	 * Sets the position of the sweepline.
	 * 
	 * @param x
	 */
	public void setSweepline(double x);

	/**
	 * Refreshes the algorithm
	 */
	public void redraw();

	/**
	 * Clears all the visuals.
	 */
	public void clean();
}
