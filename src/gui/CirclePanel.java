package gui;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.Semaphore;

import javax.swing.JPanel;

import util.Concurrency;
import core.Box;
import core.Circle;
import core.Point;

/**
 * Panel which visualizes the circle intersection algorithm.
 * 
 * @author Niels Billen
 * @version 0.1
 */
public class CirclePanel extends JPanel implements CircleAlgorithmVisualizer {
	private static final long serialVersionUID = 7892498154960839365L;

	private final TMIGui frame;

	private Set<Circle> circles = new HashSet<Circle>();
	private Map<Circle, CircleHighlight> highlight = new HashMap<Circle, CircleHighlight>();
	private Set<Point> points = new HashSet<Point>();

	private double sweepLine = Double.POSITIVE_INFINITY;
	private Box bounds;

	private int intersectionTests = 0;
	private Font font = new Font("Helvetica", Font.PLAIN, 16);

	private final Semaphore lock = new Semaphore(1);

	/**
	 * 
	 * @param frame
	 * @throws NullPointerException
	 */
	public CirclePanel(TMIGui frame) throws NullPointerException {
		if (frame == null)
			throw new NullPointerException("the given frame is null!");
		this.frame = frame;
	}

	/**
	 * 
	 * @return
	 */
	public TMIGui getFrame() {
		return frame;
	}

	/**
	 * 
	 * @param circles
	 */
	public void setCircles(List<Circle> circles) {
		Box b = new Box();
		for (Circle c : circles)
			b = b.union(c.getBox());

		this.bounds = b;
		this.circles.clear();
		this.circles.addAll(circles);

		redraw();
	}

	/**
	 * 
	 * @return
	 */
	public List<Circle> getCircles() {
		return new ArrayList<Circle>(circles);
	}

	/**
	 * Returns the bounding box of all the circles in the circle panel.
	 * 
	 * @return the bounding box of all the circles in the circle panel.
	 */
	public Box getBox() {
		return bounds;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.swing.JComponent#getPreferredSize()
	 */
	@Override
	public Dimension getPreferredSize() {
		return new Dimension(800, 640);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.swing.JComponent#paint(java.awt.Graphics)
	 */
	@Override
	public void paint(Graphics g) {
		final Graphics2D g2d = (Graphics2D) g;

		// Enable anti-aliasing
		g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
				RenderingHints.VALUE_ANTIALIAS_ON);
		g2d.setRenderingHint(RenderingHints.KEY_FRACTIONALMETRICS,
				RenderingHints.VALUE_FRACTIONALMETRICS_ON);

		// Calculate the offsets and scales.
		final Box b = getBox();
		final Dimension d = getSize();

		final int minPanelSize = Math.min(d.width, d.height) - 64;
		final double maxCircleBound = Math.max(b.getWidth(), b.getHeight());
		final double scale = (double) minPanelSize / (double) maxCircleBound;

		final int xOff = (int) (0.5 * (d.width - scale * b.getWidth()));
		final int yOff = (int) (0.5 * (d.height - scale * b.getHeight()));

		g2d.setColor(Color.WHITE);
		g2d.fillRect(0, 0, d.width, d.height);

		g2d.setColor(Color.BLUE);
		int x = (int) (xOff + scale * (sweepLine - b.min.x));
		g2d.drawLine(x, 0, x, d.height);
		g2d.setColor(Color.BLACK);

		// Draw the circles
		for (Circle c : circles)
			drawCircle(g2d, c, xOff, yOff, -b.min.x, -b.min.y, scale);

		// Make a thread safe copy of the highlights
		Concurrency.lock(lock);
		Set<Entry<Circle, CircleHighlight>> s = new HashSet<Entry<Circle, CircleHighlight>>(
				highlight.entrySet());
		Set<Point> pointCopy = new HashSet<Point>(points);
		Concurrency.release(lock);

		// Draw the highlights
		for (Entry<Circle, CircleHighlight> e : s) {
			Circle c = e.getKey();
			CircleHighlight h = e.getValue();

			g2d.setColor(h.color);
			g2d.setStroke(h.stroke);
			drawCircle(g2d, c, xOff, yOff, -b.min.x, -b.min.y, scale);
		}

		// Draw the found intersections
		g2d.setColor(Color.GRAY);
		g2d.setStroke(new BasicStroke(1.f));
		double inv_scale = 1.0 / scale;
		for (Point p : pointCopy)
			fillCircle(g2d, new Circle(p, 3 * inv_scale), xOff, yOff, -b.min.x,
					-b.min.y, scale);

		g2d.setColor(Color.BLACK);
		g2d.setFont(font);

		String string = String.format(Locale.ENGLISH,
				"intersections tests: %d", intersectionTests);
		FontMetrics metrics = g2d.getFontMetrics();
		Rectangle2D r = metrics.getStringBounds(string, g2d);

		g2d.drawString(string, 16, (int) (d.height - r.getHeight()));

	}

	/**
	 * 
	 * @param g2d
	 * @param c
	 * @param cXOff
	 * @param cYOff
	 * @param scale
	 */
	private void drawCircle(Graphics2D g2d, Circle c, int xOff, int yOff,
			double cXOff, double cYOff, double scale) {

		int x = (int) Math.round(scale * (c.p.x - c.r + cXOff));
		int y = (int) Math.round(scale * (c.p.y - c.r + cYOff));
		int w = (int) Math.ceil(scale * 2 * c.r);

		g2d.drawArc(xOff + x, yOff + y, w, w, 0, 360);
	}

	/**
	 * 
	 * @param g2d
	 * @param c
	 * @param cXOff
	 * @param cYOff
	 * @param scale
	 */
	private void fillCircle(Graphics2D g2d, Circle c, int xOff, int yOff,
			double cXOff, double cYOff, double scale) {

		int x = (int) Math.round(scale * (c.p.x - c.r + cXOff));
		int y = (int) Math.round(scale * (c.p.y - c.r + cYOff));
		int w = (int) Math.ceil(scale * 2 * c.r);

		g2d.fillArc(xOff + x, yOff + y, w, w, 0, 360);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see gui.CircleAlgorithmVisualizer#addHighlight(core.Circle,
	 * gui.CircleHighlight)
	 */
	@Override
	public void addHighlight(Circle c, CircleHighlight h) {
		Concurrency.lock(lock);
		highlight.put(c, h);
		Concurrency.release(lock);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see gui.CircleAlgorithmVisualizer#removeHighlight(core.Circle)
	 */
	@Override
	public void removeHighlight(Circle circle) {
		Concurrency.lock(lock);
		highlight.remove(circle);
		Concurrency.release(lock);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see gui.CircleAlgorithmVisualizer#clearHightlight()
	 */
	@Override
	public void clearHightlight() {
		Concurrency.lock(lock);
		highlight.clear();
		Concurrency.release(lock);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see gui.CircleAlgorithmVisualizer#setSweepline(double)
	 */
	@Override
	public void setSweepline(double x) {
		sweepLine = x;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see gui.CircleAlgorithmVisualizer#redraw()
	 */
	@Override
	public void redraw() {
		repaint();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see gui.CircleAlgorithmVisualizer#addPoint(Point)
	 */
	@Override
	public void addPoint(Point point) {
		Concurrency.lock(lock);
		points.add(point);
		Concurrency.release(lock);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see gui.CircleAlgorithmVisualizer#clean()
	 */
	@Override
	public void clean() {
		Concurrency.lock(lock);
		highlight.clear();
		sweepLine = Double.POSITIVE_INFINITY;
		redraw();
		Concurrency.release(lock);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see gui.CircleAlgorithmVisualizer#setIntersectionTests(int)
	 */
	@Override
	public void setIntersectionTests(int intersectionTests) {
		Concurrency.lock(lock);
		this.intersectionTests = intersectionTests;
		Concurrency.release(lock);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see gui.CircleAlgorithmVisualizer#clearPoints()
	 */
	@Override
	public void clearPoints() {
		points.clear();
	}
}
