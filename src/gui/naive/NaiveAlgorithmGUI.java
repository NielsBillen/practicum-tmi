package gui.naive;

import gui.CircleAlgorithmVisualizer;
import gui.VisualCircleIntersectionAlgorithm;

import java.util.List;

import core.Circle;
import core.Point;

/**
 * 
 * @author Niels Billen
 * @version 0.1
 */
public class NaiveAlgorithmGUI extends VisualCircleIntersectionAlgorithm {
	private int s;
	private int i;
	private int j;

	/**
	 * 
	 * @param circles
	 */
	public NaiveAlgorithmGUI() {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see gui.VisualCircleIntersectionAlgorithm#setCircles(java.util.List)
	 */
	@Override
	public void setCircles(List<Circle> circles) {
		super.setCircles(circles);

		s = circles.size();
		i = 0;
		j = Math.min(1, s);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see algorithm.CircleIntersectionAlgorithm#isFinished()
	 */
	@Override
	public boolean isFinished() {
		return i == s - 1 && j == s;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * gui.CircleIntersectionAlgorithmGUI#doStep(gui.CircleAlgorithmVisualizer)
	 */
	@Override
	public void doStep(CircleAlgorithmVisualizer visualizer) {
		if (isFinished())
			return;
		Circle c1 = circles.get(i);
		Circle c2 = circles.get(j);
		++tests;

		for (Point p : c1.Intersect(c2)) {
			if (visualizer != null)
				visualizer.addPoint(p);
			intersections.add(p);
		}

		if (visualizer != null) {
			visualizer.clearHightlight();
			visualizer.addHighlight(c1, GREEN_THICK);
			visualizer.addHighlight(c2, RED_THICK);
			visualizer.setIntersectionTests(tests);
			visualizer.redraw();
		}

		++j;
		if (j >= s) {
			++i;
			j = i + 1;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see algorithm.CircleIntersectionAlgorithm#getName()
	 */
	@Override
	public String getName() {
		return "NaiveAlgorithm";
	}
}
