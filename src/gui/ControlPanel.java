package gui;

import gui.bvh.BVHAlgorithmGUI;
import gui.naive.NaiveAlgorithmGUI;
import gui.sweepline.SweepLineAlgorithmGUI;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.concurrent.Semaphore;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.border.TitledBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import util.CircleGenerator;
import util.Concurrency;

/**
 * Panel with the controls for the algorithm.
 * 
 * @author Niels Billen
 * @version 0.1
 */
public class ControlPanel extends JPanel {
	private static final long serialVersionUID = 4830132842161733309L;

	private final TMIGui frame;
	private final JButton start;
	private final JButton pauze;
	private final JComboBox box;
	private final JSlider slider;
	private final JSlider circles;

	private final Semaphore lock = new Semaphore(1);

	public final static int MAX_STEPS_PER_SECOND = 1000;
	public final static int MINIMUM_NB_OF_CIRCLES = 10;
	public final static int MAXIMUM_NB_OF_CIRCLES = 1000;
	public final static int DEFAULT_NB_OF_CIRCLES = 20;

	private ControlThread control = null;
	private boolean finished = true; // Whether the algorithm has finished
										// executing

	/**
	 * 
	 * @param frame
	 */
	public ControlPanel(TMIGui frame) {
		this.frame = frame;

		start = new JButton("Start");
		pauze = new JButton("Pauze");
		slider = new JSlider(1, MAX_STEPS_PER_SECOND);
		circles = new JSlider(MINIMUM_NB_OF_CIRCLES, MAXIMUM_NB_OF_CIRCLES);

		box = new JComboBox();
		box.addItem("Naive algorithm");
		box.addItem("Sweep line algorithm");
		box.addItem("Bounding volume hierachy algorithm");

		JPanel controls = new JPanel() {
			private static final long serialVersionUID = -8127471717311186375L;

			/*
			 * (non-Javadoc)
			 * 
			 * @see javax.swing.JComponent#getPreferredSize()
			 */
			@Override
			public Dimension getPreferredSize() {
				return new Dimension(480, 56);

			}
		};
		JPanel circlePanel = new JPanel() {
			private static final long serialVersionUID = 4537809102754328878L;

			/*
			 * (non-Javadoc)
			 * 
			 * @see javax.swing.JComponent#getPreferredSize()
			 */
			@Override
			public Dimension getPreferredSize() {
				int width = ControlPanel.this.getSize().width - 32 - 480;
				return new Dimension(width / 2, 56);
			}
		};
		JPanel speed = new JPanel() {
			private static final long serialVersionUID = 4537809102754328878L;

			/*
			 * (non-Javadoc)
			 * 
			 * @see javax.swing.JComponent#getPreferredSize()
			 */
			@Override
			public Dimension getPreferredSize() {
				int width = ControlPanel.this.getSize().width - 32 - 480;
				return new Dimension(width / 2, 56);
			}
		};

		controls.setBorder(new TitledBorder("Control"));
		circlePanel.setBorder(new TitledBorder("Number of circles"));
		speed.setBorder(new TitledBorder("Execution Speed"));

		controls.setLayout(new FlowLayout(FlowLayout.CENTER, 4, 0));
		circlePanel.setLayout(new GridLayout(1, 2, 4, 16));
		speed.setLayout(new GridLayout(1, 2, 4, 16));

		controls.add(box);
		controls.add(start);
		controls.add(pauze);
		circlePanel.add(circles);
		speed.add(slider);

		add(controls);
		add(circlePanel);
		add(speed);

		pauze.setEnabled(false);
		slider.setValue(0);
		circles.setValue(DEFAULT_NB_OF_CIRCLES);

		initializeListeners();
	}

	/**
	 * 
	 */
	private void initializeListeners() {
		start.addActionListener(new ActionListener() {
			/*
			 * (non-Javadoc)
			 * 
			 * @see
			 * java.awt.event.ActionListener#actionPerformed(java.awt.event.
			 * ActionEvent)
			 */
			@Override
			public void actionPerformed(ActionEvent e) {
				algorithmStart();
			}
		});
		pauze.addActionListener(new ActionListener() {
			/*
			 * (non-Javadoc)
			 * 
			 * @see
			 * java.awt.event.ActionListener#actionPerformed(java.awt.event.
			 * ActionEvent)
			 */
			@Override
			public void actionPerformed(ActionEvent e) {
				algorithmPauze();
			}
		});
		slider.addChangeListener(new ChangeListener() {
			/*
			 * (non-Javadoc)
			 * 
			 * @see
			 * javax.swing.event.ChangeListener#stateChanged(javax.swing.event
			 * .ChangeEvent)
			 */
			@Override
			public void stateChanged(ChangeEvent e) {
				setStepsPerSecond(slider.getValue());
			}
		});
		circles.addChangeListener(new ChangeListener() {
			/*
			 * (non-Javadoc)
			 * 
			 * @see
			 * javax.swing.event.ChangeListener#stateChanged(javax.swing.event
			 * .ChangeEvent)
			 */
			@Override
			public void stateChanged(ChangeEvent e) {
				frame.setCircles(CircleGenerator.generate(0,
						circles.getValue(), 10));

			}
		});
		box.addItemListener(new ItemListener() {
			/*
			 * (non-Javadoc)
			 * 
			 * @see
			 * java.awt.event.ItemListener#itemStateChanged(java.awt.event.ItemEvent
			 * )
			 */
			@Override
			public void itemStateChanged(ItemEvent e) {
				if (e.getStateChange() == ItemEvent.SELECTED)
					setAlgorithm(box.getSelectedIndex(), true);
			}
		});
	}

	/**
	 * Starts the algorithm.
	 * 
	 * This can only happen when there is no thread controlling the execution.
	 * The "finished" field is used to differentiate between a pauzed and
	 * finished algorithm.
	 */
	private void algorithmStart() {
		Concurrency.lock(lock);

		if (control == null) {
			if (finished) {
				frame.getCirclePanel().clearPoints();
				frame.getCirclePanel().setIntersectionTests(0);
				// Create a new stateless algorithm
				setAlgorithm(box.getSelectedIndex(), false);
			}
			finished = false;
			start.setEnabled(false);
			circles.setEnabled(false);
			pauze.setEnabled(true);
			control = new ControlThread(slider.getValue());
			control.start();
		}

		Concurrency.release(lock);
	}

	/**
	 * 
	 */
	private void algorithmPauze() {
		Concurrency.lock(lock);

		if (control != null) {
			pauze.setEnabled(false);
			control.stopThread();
			while (control.isRunning())
				Thread.yield();
			control = null;
			start.setEnabled(true);
		}

		Concurrency.release(lock);
	}

	/**
	 * 
	 */
	private void algorithmFinished() {
		Concurrency.lock(lock);

		// Clean the highlights
		frame.getCirclePanel().clean();
		frame.getCirclePanel().redraw();

		// Modify control.
		control = null;
		finished = true;

		// Enable/Disable buttons
		start.setEnabled(true);
		circles.setEnabled(true);
		pauze.setEnabled(false);

		Concurrency.release(lock);
	}

	/**
	 * 
	 * @param stepsPerSecond
	 */
	private void setStepsPerSecond(int stepsPerSecond) {
		Concurrency.lock(lock);
		if (control != null)
			control.setStepsPerSecond(slider.getValue());
		Concurrency.release(lock);
	}

	/**
	 * 
	 * @param algorithm
	 */
	private void setAlgorithm(int index, boolean aquireLock) {
		if (aquireLock)
			Concurrency.lock(lock);
		if (!finished || control != null) {
			// do nothing
		} else if (index == 0)
			frame.setAlgorithm(new NaiveAlgorithmGUI());
		else if (index == 1)
			frame.setAlgorithm(new SweepLineAlgorithmGUI());
		else if (index == 2)
			frame.setAlgorithm(new BVHAlgorithmGUI());
		if (aquireLock)
			Concurrency.release(lock);
	}

	/**
	 * 
	 * @author Niels Billen
	 * @version 0.1
	 */
	private class ControlThread extends Thread {
		private boolean stop = false;
		private boolean isRunning = false;
		private long stepsPerSecond;

		/**
		 * 
		 * @param stepsPerSecond
		 */
		public ControlThread(long stepsPerSecond) {
			this.stepsPerSecond = stepsPerSecond;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see java.lang.Thread#run()
		 */
		@Override
		public void run() {
			isRunning = true;
			while (!stop && !frame.getAlgorithm().isFinished()) {
				frame.getAlgorithm().doStep(frame.getCirclePanel());

				if (stepsPerSecond == MAX_STEPS_PER_SECOND)
					Thread.yield();
				else
					try {
						Thread.sleep(1000 / stepsPerSecond);
					} catch (InterruptedException e) {
					}
			}

			if (frame.getAlgorithm().isFinished()) {
				algorithmFinished();
			}
			isRunning = false;
		}

		/**
		 * 
		 * @return
		 */
		public boolean isRunning() {
			return isRunning;
		}

		/**
		 * 
		 */
		public void stopThread() {
			stop = true;
		}

		/**
		 * 
		 * @param stepsPerSecond
		 */
		public void setStepsPerSecond(long stepsPerSecond) {
			this.stepsPerSecond = stepsPerSecond;
		}
	}
}
