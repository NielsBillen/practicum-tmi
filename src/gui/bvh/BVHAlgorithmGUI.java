package gui.bvh;

import gui.CircleAlgorithmVisualizer;
import gui.VisualCircleIntersectionAlgorithm;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.Semaphore;

import util.Concurrency;

import algorithm.bvh.BVHBuildInfo;
import algorithm.bvh.BVHNode;
import core.Box;
import core.Circle;
import core.Point;

/**
 * Algorithm which uses a Bounding Volume Hierarchy to find the relevant
 * intersecting circles in E * log(N) time.
 * 
 * @author Niels Billen
 * @version 1.0
 */
public class BVHAlgorithmGUI extends VisualCircleIntersectionAlgorithm {
	private int index = 0;
	private List<BVHNode> nodes;

	private int todoIndex = 0;
	private List<Circle> todo = new ArrayList<Circle>();

	private Semaphore lock = new Semaphore(1);

	/**
	 * Creates a new BVHAlgorithm.
	 */
	public BVHAlgorithmGUI() {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see gui.VisualCircleIntersectionAlgorithm#setCircles(java.util.List)
	 */
	@Override
	public void setCircles(List<Circle> cc) {
		Concurrency.lock(lock);
		super.setCircles(cc);

		List<BVHBuildInfo> info = new ArrayList<BVHBuildInfo>(circles.size());
		for (int i = 0; i < circles.size(); ++i)
			info.add(new BVHBuildInfo(i, circles.get(i).getBox()));

		int c = circles.size();
		int s = c * (int) Math.ceil((Math.log(c) / Math.log(2)));

		nodes = new ArrayList<BVHNode>(s);
		BVHNode.buildBVH(nodes, info, 0, info.size());
		
		index = 0;
		todoIndex = 0;
		todo.clear();

		Concurrency.release(lock);
	}

	/**
	 * 
	 * @param nodes
	 * @param circle
	 * @return
	 */
	private List<Circle> findOverlapping(List<BVHNode> nodes,
			List<Circle> circles, Circle circle) {
		Box b = circle.getBox();

		List<Circle> overlapping = new ArrayList<Circle>();
		LinkedList<Integer> todo = new LinkedList<Integer>();
		todo.add(0);

		BVHNode n;
		while (!todo.isEmpty()) {
			final int nodeId = todo.pop();
			n = nodes.get(nodeId);

			if (b.overlaps(n.bound)) {
				if (n.isLeaf == 0) {
					todo.add(nodeId + 1);
					todo.add(n.index);
				} else {
					final Circle c = circles.get(n.index);
					if (c != circle && circle.id < c.id)
						overlapping.add(c);
				}
			}
		}

		return overlapping;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see algorithm.CircleIntersectionAlgorithm#isFinished()
	 */
	@Override
	public boolean isFinished() {
		return index >= circles.size() && todoIndex >= todo.size();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * gui.CircleIntersectionAlgorithmGUI#doStep(gui.CircleAlgorithmVisualizer)
	 */
	@Override
	public void doStep(CircleAlgorithmVisualizer visualizer) {
		Concurrency.lock(lock);
		if (isFinished()) {
			Concurrency.release(lock);
			return;
		}

		if (todoIndex < todo.size()) {
			Circle c1 = circles.get(index - 1);
			Circle c2 = todo.get(todoIndex++);

			if (c1.id < c2.id) {
				++tests;

				for (Point p : c1.Intersect(c2)) {
					if (visualizer != null)
						visualizer.addPoint(p);
					intersections.add(p);
				}
			}

			if (visualizer != null) {
				visualizer.setIntersectionTests(tests);
				visualizer.clearHightlight();
				visualizer.addHighlight(c1, GREEN_THICK);
				for (Circle c : todo)
					visualizer.addHighlight(c, BLUE_THICK);
				visualizer.addHighlight(c2, RED_THICK);
				visualizer.redraw();
			}

		} else {
			Circle circle = circles.get(index++);
			todo = findOverlapping(nodes, circles, circle);
			todoIndex = 0;

			if (visualizer != null) {
				visualizer.clearHightlight();
				for (Circle c : todo)
					visualizer.addHighlight(c, BLUE_THICK);
				visualizer.addHighlight(circle, GREEN_THICK);
				visualizer.redraw();
			}
		}
		Concurrency.release(lock);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see algorithm.CircleIntersectionAlgorithm#getName()
	 */
	@Override
	public String getName() {
		return "BVHAlgorithm";
	}
}
