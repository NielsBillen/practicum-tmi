package gui;

import gui.naive.NaiveAlgorithmGUI;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.Toolkit;
import java.util.Collections;
import java.util.List;

import javax.swing.JFrame;

import util.CircleGenerator;
import core.Circle;

/**
 * 
 * @author Niels Billen
 * @version 0.1
 */
public class TMIGui extends JFrame {
	private static final long serialVersionUID = -2476846883986517372L;
	private CirclePanel panel;
	private ControlPanel controls;
	private VisualCircleIntersectionAlgorithm algorithm = new NaiveAlgorithmGUI();

	/**
	 * 
	 */
	public TMIGui() {
		super("Practicum Toepassingen van de meetkunde in de informatica");

		panel = new CirclePanel(this);
		controls = new ControlPanel(this);

		setCircles(CircleGenerator.generate(0,
				ControlPanel.DEFAULT_NB_OF_CIRCLES, 10));

		// Add content and pack
		add(panel, BorderLayout.CENTER);
		add(controls, BorderLayout.SOUTH);
		pack();

		// Center the gui
		GraphicsEnvironment g = GraphicsEnvironment
				.getLocalGraphicsEnvironment();
		GraphicsDevice[] d = g.getScreenDevices();

		Dimension screen;
		if (d.length == 0)
			screen = Toolkit.getDefaultToolkit().getScreenSize();
		else
			screen = d[0].getDefaultConfiguration().getBounds().getSize();
		Dimension size = getSize();

		setLocation((screen.width - size.width) / 2,
				(screen.height - size.height) / 2);

		setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

	}

	/**
	 * 
	 * @return
	 */
	public CirclePanel getCirclePanel() {
		return panel;
	}

	/**
	 * 
	 * @return
	 */
	public ControlPanel getControlPanel() {
		return controls;
	}

	/**
	 * 
	 * @param algorithm
	 */
	public void setAlgorithm(VisualCircleIntersectionAlgorithm algorithm) {
		if (algorithm == null)
			throw new NullPointerException("the given algorithm is null!");
		this.algorithm = algorithm;
		this.algorithm.setCircles(panel.getCircles());
	}

	/**
	 * 
	 */
	public void setCircles(List<Circle> circles) {
		Collections.sort(circles);
		this.panel.setCircles(circles);
		this.panel.clean();
		this.panel.clearPoints();
		this.algorithm.setCircles(circles);
	}

	/**
	 * 
	 * @return
	 */
	public VisualCircleIntersectionAlgorithm getAlgorithm() {
		return algorithm;
	}
}
