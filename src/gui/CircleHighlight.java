package gui;

import java.awt.BasicStroke;
import java.awt.Color;

/**
 * Wrapper for storing highlight information for a circle.
 * 
 * @author Niels Billen
 * @version 0.1
 */
public class CircleHighlight {
	public final Color color;
	public final BasicStroke stroke;

	/**
	 * 
	 * @param color
	 * @param strokeWidth
	 */
	public CircleHighlight(Color color, float strokeWidth) {
		this.color = color;
		this.stroke = new BasicStroke(strokeWidth);
	}
}
