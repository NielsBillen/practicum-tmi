package gui.sweepline;

import gui.CircleAlgorithmVisualizer;
import gui.VisualCircleIntersectionAlgorithm;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import algorithm.linesweep.CircleEvent;
import algorithm.linesweep.CircleEventType;
import algorithm.linesweep.SweepLine;
import core.Circle;
import core.Point;

/**
 * A sweep line algoritm which reduces the required number of intersection tests
 * by keeping an active set of circles.
 * 
 * The worst case number of intersections remains O(N^2) which occurs when
 * roughly all the circles are active.
 * 
 * @author Niels Billen
 * @version 0.1
 */
public class SweepLineAlgorithmGUI extends VisualCircleIntersectionAlgorithm {
	private List<CircleEvent> events;
	private int index = 0;
	private SweepLine line = new SweepLine();
	private LinkedList<Circle> todo = new LinkedList<Circle>();
	private Circle lastCircle = null;

	/**
	 * 
	 */
	public SweepLineAlgorithmGUI() {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see gui.VisualCircleIntersectionAlgorithm#setCircles(java.util.List)
	 */
	@Override
	public void setCircles(List<Circle> circles) {
		super.setCircles(circles);

		events = new ArrayList<CircleEvent>(2 * circles.size());
		CircleEvent start, end;
		for (Circle c : circles) {
			start = new CircleEvent(c.p.x - c.r, c, CircleEventType.START);
			end = new CircleEvent(c.p.x + c.r, c, CircleEventType.END);
			events.add(start);
			events.add(end);
		}
		Collections.sort(events);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see algorithm.CircleIntersectionAlgorithm#isFinished()
	 */
	@Override
	public boolean isFinished() {
		return index == events.size() && todo.isEmpty();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * gui.CircleIntersectionAlgorithmGUI#doStep(gui.CircleAlgorithmVisualizer)
	 */
	@Override
	public void doStep(CircleAlgorithmVisualizer visualizer) {
		if (isFinished())
			return;

		if (!todo.isEmpty()) {
			Circle c = todo.pop();
			++tests;
			for (Point p : lastCircle.Intersect(c)) {
				if (visualizer != null)
					visualizer.addPoint(p);
				intersections.add(p);
			}

			if (visualizer != null) {
				visualizer.clearHightlight();
				visualizer.setIntersectionTests(tests);

				for (Circle cc : line.getCircles())
					visualizer.addHighlight(cc, BLUE_THICK);

				visualizer.addHighlight(c, RED_THICK);
				visualizer.addHighlight(lastCircle, GREEN_THICK);
				visualizer.redraw();
			}
		} else {
			CircleEvent e = events.get(index++);

			if (e.type == CircleEventType.START) {
				todo.addAll(line.getCircles());
				line.add(e.circle);
				lastCircle = e.circle;
			} else
				line.remove(e.circle);

			if (visualizer != null) {
				visualizer.clearHightlight();
				for (Circle cc : line.getCircles())
					visualizer.addHighlight(cc, BLUE_THICK);
				visualizer.addHighlight(lastCircle, GREEN_THICK);
				visualizer.setSweepline(e.x);
				visualizer.redraw();
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see algorithm.CircleIntersectionAlgorithm#getName()
	 */
	@Override
	public String getName() {
		return "LineSweepAlgorithm";
	}
}
