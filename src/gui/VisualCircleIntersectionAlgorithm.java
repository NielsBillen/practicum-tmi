package gui;

import java.awt.Color;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import core.Circle;
import core.Point;

/**
 * Interactive circle intersection algorithm.
 * 
 * @author Niels Billen
 * @version 0.1
 */
public abstract class VisualCircleIntersectionAlgorithm {
	// Highlight colors.
	public static final CircleHighlight BLUE_THICK = new CircleHighlight(
			Color.BLUE, 5);
	public static final CircleHighlight RED_THICK = new CircleHighlight(
			Color.RED, 5);
	public static final CircleHighlight GREEN_THICK = new CircleHighlight(
			Color.GREEN, 5);

	//
	protected final List<Circle> circles = new ArrayList<Circle>();
	protected final Set<Point> intersections = new HashSet<Point>();
	protected int tests = 0;

	/**
	 * 
	 * @param circles
	 */
	public VisualCircleIntersectionAlgorithm() {
	}

	/**
	 * 
	 * @param circles
	 */
	public void setCircles(List<Circle> circles) {
		this.circles.clear();
		this.circles.addAll(circles);
	}

	/**
	 * 
	 * @return
	 */
	public abstract boolean isFinished();

	/**
	 * 
	 */
	public abstract void doStep(CircleAlgorithmVisualizer visualizer);

	/**
	 * 
	 */
	public abstract String getName();

	/**
	 * 
	 * @return
	 */
	public int getNbOfIntersectionTests() {
		return tests;
	}

	/**
	 * 
	 * @return
	 */
	public List<Point> getIntersections() {
		return new ArrayList<Point>(intersections);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return String.format("[%s]", getName());
	}
}
