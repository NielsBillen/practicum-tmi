package algorithm.bvh;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import util.Timer;

import core.Box;
import core.Circle;
import core.Point;
import algorithm.AlgorithmResult;
import algorithm.CircleIntersectionAlgorithm;

/**
 * An algorithm which uses a binary tree to find the overlapping circles in
 * E*log(N) time.
 * 
 * @author Niels Billen
 * @version 0.1
 */
public class BVHAlgorithm implements CircleIntersectionAlgorithm {
	/*
	 * (non-Javadoc)
	 * 
	 * @see algorithm.CircleIntersectionAlgorithm#Intersect(java.util.List)
	 */
	@Override
	public AlgorithmResult Intersect(List<Circle> circles) {
		long intersectionTests = 0;
		Timer timer = new Timer(true);

		/*
		 * Build the Bounding Volume Hierarchy.
		 */
		int nodeSize = (int) (circles.size() * Math.log(circles.size()));
		List<BVHNode> nodes = new ArrayList<BVHNode>(nodeSize);
		List<BVHBuildInfo> info = new ArrayList<BVHBuildInfo>(circles.size());
		for (Circle c : circles)
			info.add(new BVHBuildInfo(info.size(), c.getBox()));
		BVHNode.buildBVH(nodes, info, 0, info.size());
		double preprocessTime = timer.lapTime();

		/*
		 * Find the intersections
		 */
		List<Point> intersections = new ArrayList<Point>();
		for (Circle circle : circles)
			intersectionTests += findIntersections(nodes, circles, circle,
					intersections);
		double executionTime = timer.lapTime();
		return new AlgorithmResult(this, intersections, intersectionTests,
				executionTime, preprocessTime);
	}

	/**
	 * Finds the relevant intersections for the given circle, with the circles
	 * specified in the the list with circles, using the bounding volume
	 * hierarchy.
	 * 
	 * @param nodes
	 * @param circles
	 * @param circle
	 * @param intersections
	 * @return
	 */
	private long findIntersections(List<BVHNode> nodes, List<Circle> circles,
			Circle circle, List<Point> intersections) {
		final Box b = circle.getBox();
		long intersectionTests = 0;

		final LinkedList<Integer> stack = new LinkedList<Integer>();
		stack.add(0);

		BVHNode n;
		while (!stack.isEmpty()) {
			final int nodeId = stack.pop();
			n = nodes.get(nodeId);

			if (b.overlaps(n.bound)) {
				if (n.isLeaf == 0) {
					stack.add(nodeId + 1);
					stack.add(n.index);
				} else {
					final Circle c = circles.get(n.index);
					if (circle.id < c.id) {
						++intersectionTests;
						for (Point p : circle.Intersect(c))
							intersections.add(p);
					}
				}
			}
		}

		return intersectionTests;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see algorithm.CircleIntersectionAlgorithm#getName()
	 */
	@Override
	public String getName() {
		return "BVHAlgorithm";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return String.format("[%s]", getName());
	}
}
