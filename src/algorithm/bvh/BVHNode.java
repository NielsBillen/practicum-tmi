package algorithm.bvh;

import java.util.Comparator;
import java.util.List;

import util.Sort;
import core.Box;

/**
 * Represents a node in a bounding volume hierarchy.
 * 
 * The nodes should be stored in an array in heap order. This means that for a
 * tree node, the left child is stored next to this node, and the right child
 * should be stored at the array position "index".
 * 
 * When the node is a leaf node, "index" is the index of the element in the
 * array of elements stored in the tree.
 * 
 * The node stores also stores its own "id", which is its index in the array.
 * 
 * The filler integer makes sure that the size of the structure is 48 bytes.
 * 
 * @author Niels Billen
 * @version 0.1
 */
public class BVHNode {
	public Box bound = new Box(); // 4 * 64 bit
	public int index; // 32 bit
	public int isLeaf; // 32 bit

	/**
	 * Creates a new uninitialized bounding volume hierarchy node.
	 */
	public BVHNode() {
		this.index = -1;
		this.isLeaf = -1;
	}

	/**
	 * Builds a bounding volume hierarchy for the items referenced by the
	 * BVHBuildInfo objects.
	 * 
	 * 
	 * 
	 * @param nodes
	 * @param info
	 * @param start
	 * @param end
	 */
	public static void buildBVH(List<BVHNode> nodes, List<BVHBuildInfo> info,
			int start, int end) throws IllegalArgumentException {
		final int size = end - start;

		BVHNode node = new BVHNode();
		nodes.add(node);

		if (size < 1)
			throw new IllegalArgumentException("cannot build BVH tree for "
					+ size + " elements!");
		else if (size == 1) {
			BVHBuildInfo i = info.get(start);
			node.index = i.index;
			node.bound = i.bound;
			node.isLeaf = 1;
		} else {
			// Calculate the bound of the node.
			for (int i = start; i < end; ++i)
				node.bound = node.bound.union(info.get(i).bound);

			int mid = (start + end) / 2;
			int axis = node.bound.getWidth() > node.bound.getHeight() ? 0 : 1;
			Comparator<BVHBuildInfo> c = BVHBuildInfo.getComparator(axis);

			Sort.sort(info, c, start, end);

			buildBVH(nodes, info, start, mid);
			node.index = nodes.size();
			node.isLeaf = 0;
			buildBVH(nodes, info, mid, end);
		}
	}
}
