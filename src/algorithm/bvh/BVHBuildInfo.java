package algorithm.bvh;

import java.util.Comparator;

import core.Box;
import core.Point;

/**
 * An object which caches information used to build a bounding volume hierarchy.
 * 
 * @author Niels Billen
 * @version 0.1
 */
public class BVHBuildInfo {
	public final int index; // index of the element this info is pointing to
	public final Box bound; // bounding box of the element
	public final Point center; // center of the bounding box

	/**
	 * Creates a bounding volume hierarchy info object.
	 * 
	 * This object stores the "index" of an element in an array and its
	 * corresponding bounding box.
	 * 
	 * @param index
	 * @param bound
	 */
	public BVHBuildInfo(int index, Box bound) {
		this.index = index;
		this.bound = bound;
		this.center = new Point(0.5 * (bound.min.x + bound.max.x),
				0.5 * (bound.min.y + bound.max.y));
	}

	/**
	 * Returns a comparator to sort BVHBuildInfo objects along an axis using the
	 * centers of the bounding boxes.
	 * 
	 * @param axis
	 *            The axis to use (0 = x-axis, 1 = y-axis)
	 * @return a comparator to sort BVHBuildInfo objects.
	 */
	public static Comparator<BVHBuildInfo> getComparator(final int axis) {
		return new Comparator<BVHBuildInfo>() {
			/*
			 * 
			 */
			@Override
			public int compare(BVHBuildInfo o1, BVHBuildInfo o2) {
				final double v1 = axis == 0 ? o1.center.x : o1.center.y;
				final double v2 = axis == 0 ? o2.center.x : o2.center.y;

				if (v1 < v2)
					return -1;
				else if (v1 > v2)
					return 1;
				else
					return 0;
			}
		};
	}
}
