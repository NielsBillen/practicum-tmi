package algorithm.linesweep;

import core.Circle;

/**
 * Represents an circle event for a line sweep algorithm.
 * 
 * An event is the start or end of a circle along the x axis. This event stores
 * reference to the circle which starts or ends, the type of event (start/end)
 * and the position.
 * 
 * @author Niels Billen
 * @version 0.1
 */
public class CircleEvent implements Comparable<CircleEvent> {
	public final CircleEventType type;
	public final double x;
	public final Circle circle;

	/**
	 * 
	 * @param x
	 * @param circle
	 * @param type
	 */
	public CircleEvent(double x, Circle circle, CircleEventType type) {
		this.x = x;
		this.circle = circle;
		this.type = type;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(CircleEvent o) {
		if (x > o.x)
			return 1;
		else if (x < o.x)
			return -1;
		else if (type == o.type)
			return 0;
		else if (type == CircleEventType.END)
			return 1;
		else
			return -1;
	}
}
