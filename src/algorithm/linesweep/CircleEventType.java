package algorithm.linesweep;

/**
 * Enumeration of the circle event types.
 * 
 * @author Niels Billen
 * @version 0.1
 */
public enum CircleEventType {
	START, END;
}
