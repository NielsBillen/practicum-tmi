package algorithm.linesweep;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import util.Timer;
import algorithm.AlgorithmResult;
import algorithm.CircleIntersectionAlgorithm;
import core.Circle;
import core.Point;

/**
 * A sweep line algoritm which reduces the required number of intersection tests
 * by keeping an active set of circles.
 * 
 * The worst case number of intersections remains O(N^2) which occurs when
 * roughly all the circles are active.
 * 
 * @author Niels Billen
 * @version 0.1
 */
public class SweepLineAlgorithm implements CircleIntersectionAlgorithm {
	/**
	 * 
	 */
	public SweepLineAlgorithm() {
	}

	/**
	 * 
	 * @param circles
	 * @return
	 */
	public AlgorithmResult Intersect(List<Circle> circles) {
		// Create the events for the Line Sweep algorithm
		long intersectionTests = 0;
		List<CircleEvent> events;
		events = new ArrayList<CircleEvent>(2 * circles.size());

		Timer timer = new Timer(true);

		CircleEvent start, end;
		for (Circle c : circles) {
			start = new CircleEvent(c.p.x - c.r, c, CircleEventType.START);
			end = new CircleEvent(c.p.x + c.r, c, CircleEventType.END);
			events.add(start);
			events.add(end);
		}

		Collections.sort(events);
		double preprocessTime = timer.lapTime();

		// Iterate over the events
		List<Point> intersections = new ArrayList<Point>();
		SweepLine line = new SweepLine();

		for (CircleEvent e : events) {
			if (e.type == CircleEventType.START) {
				for (Circle c : line) {
					++intersectionTests;
					for (Point p : e.circle.Intersect(c))
						intersections.add(p);
				}
				line.add(e.circle);
			} else
				line.remove(e.circle);
		}

		double executionTime = timer.lapTime();

		return new AlgorithmResult(this, intersections, intersectionTests,
				executionTime, preprocessTime);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see algorithm.CircleIntersectionAlgorithm#getName()
	 */
	@Override
	public String getName() {
		return "LineSweepAlgorithm";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return String.format("[%s]", getName());
	}
}
