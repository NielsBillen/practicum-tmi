package algorithm.linesweep;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import core.Circle;

/**
 * The sweepline which stores the set of active circles for the
 * SweepLineAlgorithm.
 * 
 * In order to be efficient, additions and deletions from the set should occur
 * in O(log(N)) time.
 * 
 * @author Niels Billen
 * @version 0.1
 */
public class SweepLine implements Iterable<Circle> {
	private final Set<Circle> circles = new HashSet<Circle>();

	/**
	 * 
	 */
	public SweepLine() {
	}

	/**
	 * 
	 * @param circle
	 */
	public void add(Circle circle) {
		circles.add(circle);
	}

	/**
	 * 
	 * @param circle
	 */
	public void remove(Circle circle) {
		circles.remove(circle);
	}

	/**
	 * 
	 * @return
	 */
	public Collection<Circle> getCircles() {
		return circles;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Iterable#iterator()
	 */
	@Override
	public Iterator<Circle> iterator() {
		return circles.iterator();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("[SweepLine] with " + circles.size()
				+ " active circles:");
		for (Circle c : this)
			builder.append("\n\t" + c);
		return builder.toString();
	}
}
