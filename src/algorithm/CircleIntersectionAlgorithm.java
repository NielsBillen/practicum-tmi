package algorithm;

import java.util.List;

import core.Circle;

/**
 * 
 * @author Niels Billen
 * @version 0.1
 */
public interface CircleIntersectionAlgorithm {
	/**
	 * 
	 * @param circles
	 * @return
	 */
	public AlgorithmResult Intersect(List<Circle> circles);

	/**
	 * 
	 * @return
	 */
	public String getName();
}
