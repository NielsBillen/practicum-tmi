package algorithm;

import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import core.Point;

/**
 * Wrapper for the result of a circle intersection algorithm.
 * 
 * This wrapper stores the reference to the algorithm, the found intersections,
 * the number of intersection tests performed between two circles, the execution
 * time and the time needed to build the structures needed for the algorithms
 * execution.
 * 
 * @author Niels Billen
 * @version 0.1
 */
public class AlgorithmResult implements Iterable<Point> {
	private CircleIntersectionAlgorithm algorithm;
	private List<Point> intersections;
	private long intersectionTests;
	private double executionTime;
	private double preprocessTime;

	/**
	 * 
	 * @param intersections
	 * @param executionTime
	 * @param preprocessTime
	 */
	public AlgorithmResult(CircleIntersectionAlgorithm algorithm,
			List<Point> intersections, long intersectionTests,
			double executionTime, double preprocessTime) {
		this.algorithm = algorithm;
		this.intersections = intersections;
		this.executionTime = executionTime;
		this.preprocessTime = preprocessTime;
		this.intersectionTests = intersectionTests;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder b = new StringBuilder();

		b.append("[AlgorithmResult]");
		b.append("\n\talgorithm:       " + algorithm.getName());
		b.append("\n\tfound:           " + intersections.size()
				+ " intersections...");
		b.append("\n\tusing:           " + intersectionTests
				+ " intersection tests...");
		b.append(String.format(Locale.ENGLISH,
				"\n\tpreprocess time: %.1f ms...", preprocessTime));
		b.append(String.format(Locale.ENGLISH,
				"\n\texecution time:  %.1f ms...", executionTime));
		b.append(String.format(Locale.ENGLISH,
				"\n\ttotal time:      %.1f ms...", executionTime
						+ preprocessTime));
		return b.toString();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Iterable#iterator()
	 */
	@Override
	public Iterator<Point> iterator() {
		return intersections.iterator();
	}

	/**
	 * 
	 * @return
	 */
	public double getTotalTime() {
		return preprocessTime + executionTime;
	}
}
