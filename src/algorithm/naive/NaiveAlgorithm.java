package algorithm.naive;

import java.util.ArrayList;
import java.util.List;

import util.Timer;
import algorithm.AlgorithmResult;
import algorithm.CircleIntersectionAlgorithm;
import core.Circle;
import core.Point;

/**
 * A naive algorithm which intersects every circle with each other circle.
 * 
 * The number of intersection tests between the circle will be N*(N-1)/2 =
 * O(N^2)
 * 
 * @author Niels Billen
 * @version 0.1
 */
public class NaiveAlgorithm implements CircleIntersectionAlgorithm {
	/**
	 * Instantiation of the naive algorithm.
	 */
	public NaiveAlgorithm() {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see algorithm.CircleIntersectionAlgorithm#Intersect(java.util.List)
	 */
	@Override
	public AlgorithmResult Intersect(List<Circle> circles) {
		long intersectionTests = 0;
		List<Point> intersections = new ArrayList<Point>();

		Timer timer = new Timer(true);

		final int s = circles.size();
		for (int i = 0; i < s - 1; ++i) {
			Circle c = circles.get(i);
			for (int j = i + 1; j < s; ++j, ++intersectionTests)
				for (Point p : c.Intersect(circles.get(j)))
					intersections.add(p);
		}

		timer.Stop();

		return new AlgorithmResult(this, intersections, intersectionTests,
				timer.Time(), 0);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see algorithm.CircleIntersectionAlgorithm#getName()
	 */
	@Override
	public String getName() {
		return "NaiveAlgorithm";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return String.format("[%s]", getName());
	}
}
