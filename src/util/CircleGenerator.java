package util;

import io.TMIFileParser;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import core.Circle;

/**
 * Utility for generating circles.
 * 
 * @author Niels Billen
 * @version 0.1
 */
public class CircleGenerator {
	/**
	 * Parses the circles from the given file.
	 * 
	 * @param file
	 * @return
	 */
	public static List<Circle> parse(File file) {
		return new TMIFileParser(file).getCircles();
	}

	/**
	 * Generates a random set of circles.
	 * 
	 * @param nbOfCircles
	 * @return
	 */
	public static List<Circle> generate(long seed, int nbOfCircles, double scale) {
		Random random = new Random(seed);
		double inv_size = 1.0 / Math.sqrt(nbOfCircles);

		List<Circle> circles = new ArrayList<Circle>(nbOfCircles);

		for (int i = 0; i < nbOfCircles; ++i)
			circles.add(new Circle(scale * random.nextDouble(), scale
					* random.nextDouble(), scale
					* (0.1 + 0.9 * random.nextDouble()) * inv_size));

		return circles;
	}
}
