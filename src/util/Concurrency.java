package util;

import java.util.concurrent.Semaphore;

/**
 * Utility class for locking and releasing semaphores.
 * 
 * @author Niels Billen
 * @version 1.0
 */
public class Concurrency {

	/**
	 * Locks the given semaphore.
	 * 
	 * This method blocks untill the lock is acquired.
	 * 
	 * @param semaphore
	 *            The semaphore to lock.
	 */
	public static void lock(Semaphore semaphore) {
		while (semaphore != null) {
			try {
				semaphore.acquire();
				break;
			} catch (InterruptedException e) {
			}
		}
	}

	/**
	 * Releases the given semaphore.
	 * 
	 * @param semaphore
	 *            The semaphore to release.
	 */
	public static void release(Semaphore semaphore) {
		if (semaphore != null) 
			semaphore.release();
	}
}
