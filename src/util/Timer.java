package util;

import java.util.Locale;

/**
 * Nano time timer class.
 * 
 * @author Niels Billen
 * @version 0.1
 */
public class Timer {
	private double invMillion = 1.0 / 1000000.0;
	private long startTime;
	private long elapsedTime;
	private boolean isRunning = false;

	/**
	 * 
	 */
	public Timer() {
	}

	/**
	 * 
	 * @param start
	 */
	public Timer(boolean start) {
		if (start)
			Start();
	}

	/**
	 * 
	 */
	public void Start() {
		if (!isRunning) {
			startTime = System.nanoTime();
			elapsedTime = 0;
			isRunning = true;
		}
	}

	/**
	 * 
	 */
	public void Reset() {
		startTime = System.nanoTime();
		elapsedTime = 0;
	}

	/**
	 * 
	 */
	public double lapTime() {
		if (!isRunning)
			return 0.0;

		long current = System.nanoTime();
		long laptime = current - startTime;

		startTime = current;
		elapsedTime += laptime;

		return laptime * invMillion;
	}

	/**
	 * 
	 */
	public void Stop() {
		if (isRunning) {
			isRunning = false;
			elapsedTime += System.nanoTime() - startTime;
		}
	}

	/**
	 * 
	 * @return
	 */
	public double Time() {
		if (isRunning) {
			Stop();
			double result = elapsedTime * invMillion;
			Start();
			return result;
		}
		return elapsedTime * invMillion;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return String.format(Locale.ENGLISH, "[Timer] clocked at %.5fms",
				Time());
	}
}
