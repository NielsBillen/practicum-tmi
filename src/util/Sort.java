package util;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Random;

/**
 * Utility class for partially sorting a list.
 * 
 * @author Niels Billen
 * @version 0.1
 */
public class Sort {
	private static final Random random = new Random(0);

	/**
	 * Sorts the elements from "start" (inclusive) to "end" (exclusive) in the
	 * given list using the comparator.
	 * 
	 * @param list
	 *            Elements to be sorted.
	 * @param c
	 *            Comparator for the elements.
	 * @param start
	 *            Start index to sort from in the list (inclusive)
	 * @param end
	 *            End index to sort to in the list (exclusive)
	 */
	public static <T> void sort(final List<T> list, final Comparator<T> c,
			int start, int end) {
		if (start < end) {
			int pivotIndex = start + random.nextInt(end - start);
			T pivot = list.get(pivotIndex);
			Collections.swap(list, pivotIndex, end - 1);

			int i = start;
			int j = end - 1;
			
			while (i != j) {
				if (c.compare(list.get(i), pivot) < 0) {
					i = i + 1;
				} else {
					list.set(j, list.get(i));
					list.set(i, list.get(j - 1));
					j = j - 1;
				}
			}
			list.set(j, pivot);
			sort(list, c, start, j);
			sort(list, c, j + 1, end);
		}
	}
}