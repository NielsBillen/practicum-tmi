package main;

import gui.TMIGui;
import io.TMIFileParser;

import java.io.File;
import java.util.Locale;

import algorithm.AlgorithmResult;
import algorithm.CircleIntersectionAlgorithm;
import algorithm.bvh.BVHAlgorithm;
import algorithm.linesweep.SweepLineAlgorithm;
import algorithm.naive.NaiveAlgorithm;
import core.Point;

/**
 * 
 * @author Niels Billen
 * @version 0.1
 */
public class TMI {
	/**
	 * 
	 * @param arguments
	 */
	public static void main(String[] arguments) {
		if (arguments.length == 0)
			new TMIGui();
		else {
			for (int i = 0; i < arguments.length; ++i) {
				if (arguments[i].endsWith("-help")) {
					System.out.println("usage: <files (optional)> -help");
					System.out.println("\t-help\t prints the help file");
					System.out
							.println("\t<files>\t parses the input file and finds the intersections between all the circles in the file");
					System.out
							.println("note: when no arguments are given, a graphical user interface is started.");
				} else {
					File file = new File(arguments[i]);
					if (!file.exists()) {
						System.out.println("Error: could not open file \""
								+ arguments[i] + "\"");
						continue;
					}
					TMIFileParser parser = new TMIFileParser(file);
					CircleIntersectionAlgorithm a;
					switch (parser.getAlgorithm()) {
					case 1:
						a = new NaiveAlgorithm();
						break;
					case 2:
						a = new SweepLineAlgorithm();
						break;
					case 3:
						a = new BVHAlgorithm();
						break;
					default:
						a = new NaiveAlgorithm();
						break;
					}

					AlgorithmResult r = a.Intersect(parser.getCircles());
					for (Point p : r)
						System.out.println(String.format(Locale.ENGLISH,
								"%.6f %.6f", p.x, p.y));
					System.out.println();
					System.out.println((long) r.getTotalTime());
				}
			}
		}
	}
}
