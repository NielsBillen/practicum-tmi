package core;

import java.util.Locale;

/**
 * 
 * @author Niels Billen
 * @version 0.1
 */
public class Point implements Comparable<Point> {
	public final double x;
	public final double y;

	/**
	 * 
	 * @param x
	 * @param y
	 */
	public Point(double x, double y) {
		this.x = x;
		this.y = y;
	}

	/**
	 * 
	 * @param p
	 * @throws NullPointerException
	 */
	public Point add(final Point p) throws NullPointerException {
		if (p == null)
			throw new NullPointerException("the given point is null!");
		return new Point(p.x + x, p.y + y);
	}

	/**
	 * 
	 * @param p
	 * @throws NullPointerException
	 */
	public Point subtract(final Point p) throws NullPointerException {
		if (p == null)
			throw new NullPointerException("the given point is null!");
		return new Point(p.x - x, p.y - y);
	}

	/**
	 * 
	 * @param p1
	 * @param p2
	 * @return
	 */
	public static double Distance(final Point p1, final Point p2) {
		double xDiff = p1.x - p2.x;
		double yDiff = p1.y - p2.y;
		return Math.sqrt(xDiff * xDiff + yDiff * yDiff);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return String.format(Locale.ENGLISH, "[Point] @ (%.10f,%.10f)", x, y);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(Point o) {
		if (x < o.x)
			return -1;
		else if (x > o.x)
			return 1;
		else if (y < o.y)
			return -1;
		else if (y > o.y)
			return 1;
		else
			return 0;
	}

	/**
	 * 
	 * @param p
	 * @return
	 */
	public boolean isEqualTo(Point p) {
		return Distance(this, p) < 1e-8;
	}
}
