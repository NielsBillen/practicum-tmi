package core;

/**
 * Interface which needs to be implemented by all geometrical shapes which can
 * be bounded by a 2D box.
 * 
 * @author Niels Billen
 * @version 0.1
 */
public interface Bounded {
	public Box getBox();
}
