package core;

import java.util.Locale;

/**
 * 
 * @author Niels Billen
 * @version 0.1
 */
public class Box implements Bounded {
	public final Point min;
	public final Point max;

	/**
	 * 
	 */
	public Box() {
		min = new Point(Double.POSITIVE_INFINITY, Double.POSITIVE_INFINITY);
		max = new Point(Double.NEGATIVE_INFINITY, Double.NEGATIVE_INFINITY);
	}

	/**
	 * 
	 * @param x1
	 * @param y1
	 * @param x2
	 * @param y2
	 */
	public Box(double x1, double y1, double x2, double y2) {
		min = new Point(Math.min(x1, x2), Math.min(y1, y2));
		max = new Point(Math.max(x1, x2), Math.max(y1, y2));
	}

	/**
	 * 
	 * @param p1
	 * @param p2
	 */
	public Box(Point p1, Point p2) {
		this(p1.x, p1.y, p2.x, p2.y);
	}

	/**
	 * 
	 * @param box
	 * @return
	 */
	public Box union(Box b) {
		Point nMin = new Point(Math.min(min.x, b.min.x), Math.min(min.y,
				b.min.y));
		Point nMax = new Point(Math.max(max.x, b.max.x), Math.max(max.y,
				b.max.y));
		return new Box(nMin, nMax);
	}

	/**
	 * 
	 * @return
	 */
	public double getWidth() {
		return max.x - min.x;
	}

	/**
	 * 
	 * @return
	 */
	public double getHeight() {
		return max.y - min.y;
	}

	/**
	 * 
	 * @param box
	 * @return
	 */
	public boolean overlaps(Box box) {
		if (min.x > box.max.x || max.x < box.min.x)
			return false;
		if (min.y > box.max.y || max.y < box.min.y)
			return false;
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see core.Bounded#getBox()
	 */
	@Override
	public Box getBox() {
		return this;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return String.format(Locale.ENGLISH,
				"[Box] from (%.4f,%.4f) to (%.4f,%.4f)", min.x, min.y, max.x,
				max.y);
	}
}
