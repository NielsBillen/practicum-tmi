package core;

import java.util.Locale;

/**
 * Represents a 2D circle.
 * 
 * @author Niels Billen
 * @version 0.1
 */
public class Circle implements Comparable<Circle>, Bounded {
	public final Point p;
	public final double r;

	public final int id;
	private static int next_id = 0;

	/**
	 * 
	 * @param x
	 * @param y
	 * @param r
	 */
	public Circle(double x, double y, double r) {
		this.p = new Point(x, y);
		this.r = r;
		this.id = ++next_id;
	}

	/**
	 * 
	 * @param p
	 * @param r
	 */
	public Circle(final Point p, double r) {
		this.p = p;
		this.r = r;
		this.id = ++next_id;
	}

	/**
	 * 
	 * @param c
	 * @return
	 */
	public Intersection Intersect(Circle c) {
		final double D = Point.Distance(p, c.p);

		if (D <= r + c.r && Math.abs(r - c.r) < D) {
			final double delta = 0.25 * Math.sqrt((D + r + c.r) * (D + r - c.r)
					* (D - r + c.r) * (-D + r + c.r));
			final double invD2 = 1.0 / (D * D);

			double xx = 0.5 * ((p.x + c.p.x) + (c.p.x - p.x)
					* (r * r - c.r * c.r) * invD2);
			double dx = 2 * (p.y - c.p.y) * invD2 * delta;

			double yy = 0.5 * ((p.y + c.p.y) + (c.p.y - p.y)
					* (r * r - c.r * c.r) * invD2);
			double dy = 2.0 * (p.x - c.p.x) * invD2 * delta;

			Point p1 = new Point(xx + dx, yy - dy);
			Point p2 = new Point(xx - dx, yy + dy);

			if (D == r + c.r)
				return new Intersection(p1);
			else
				return new Intersection(p1, p2);
		}
		return new Intersection();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see core.Bounded#getBox()
	 */
	@Override
	public Box getBox() {
		return new Box(p.x - r, p.y - r, p.x + r, p.y + r);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return String.format(Locale.ENGLISH,
				"[Circle]@(%.4f,%.4f) with radius %.4f", p.x, p.y, r);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(Circle o) {
		return p.compareTo(o.p);
	}
}
