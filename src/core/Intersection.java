package core;

import java.util.Arrays;
import java.util.Iterator;

/**
 * 
 * @author Niels Billen
 * @version 0.1
 */
public class Intersection implements Iterable<Point> {
	private final Point[] intersections;

	/**
	 * 
	 * @param points
	 * @throws NullPointerException
	 */
	public Intersection(Point... points) throws NullPointerException {
		if (points == null)
			throw new NullPointerException(
					"the given list of intersection points is null!");
		this.intersections = Arrays.copyOf(points, points.length);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder b = new StringBuilder();
		b.append("[Intersection]@");
		for (Point p : intersections)
			b.append("\n\t" + p);
		return b.toString();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Iterable#iterator()
	 */
	@Override
	public Iterator<Point> iterator() {
		return new Iterator<Point>() {
			private int index = 0;

			/*
			 * (non-Javadoc)
			 * 
			 * @see java.util.Iterator#hasNext()
			 */
			@Override
			public boolean hasNext() {
				return index < intersections.length;
			}

			/*
			 * (non-Javadoc)
			 * 
			 * @see java.util.Iterator#next()
			 */
			@Override
			public Point next() {
				return intersections[index++];
			}

			/*
			 * (non-Javadoc)
			 * 
			 * @see java.util.Iterator#remove()
			 */
			@Override
			public void remove() {
				throw new UnsupportedOperationException(
						"cannot remove point from intersection!");
			}
		};
	}
}
